from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, pyqtSignal
# CTP交易库
from vnctptd import *
import globalvar
import threading
from threading import Thread

VN_MOUTHNUM = 12


def log_wirtetd(filepath, content):
    with open(filepath, 'a') as wf:
        wf.write(content + "\n")
    row_cnt = globalvar.ui.table_historytd.rowCount()  # 返回当前行数（尾部）
    addrow = False
    if row_cnt > 0:
        indicatorsname = globalvar.ui.table_historytd.item(row_cnt - 1, 1).text()
        if indicatorsname != filepath:
            addrow = True
    else:
        addrow = True
    if addrow == True:
        # 增加一行
        globalvar.ui.table_historytd.insertRow(row_cnt)  # 尾部插入一行新行表格
        column_cnt = globalvar.ui.table_historytd.columnCount()  # 返回当前列数
        item = QTableWidgetItem(str(row_cnt + 1))
        globalvar.ui.table_historytd.setItem(row_cnt, 0, item)
        item = QTableWidgetItem(str(filepath))
        globalvar.ui.table_historytd.setItem(row_cnt, 1, item)


def log_todaytd(mystr):
    _translate = QtCore.QCoreApplication.translate
    item = QtWidgets.QListWidgetItem()
    globalvar.ui.list_listtdlog.addItem(item)
    item = globalvar.ui.list_listtdlog.item(globalvar.ui.list_listtdlog.count() - 1)
    tstr = time.strftime("%Y-%m-%d %H:%M:%S ", time.localtime())
    item.setText(_translate("MainWindow", tstr + mystr))
    filepath = time.strftime("logfile\\td\\%Y%m%d.txt", time.localtime())
    log_wirtetd(filepath, tstr + mystr)


global UIUpdaterealtimecurveThreadjs
UIUpdaterealtimecurveThreadjs = -1


def update_account(a):
    # print(a.TradingDay, str(a.Available))
    row_cnt = globalvar.ui.table_account.rowCount()
    item = QTableWidgetItem(str(a.BrokerID, encoding="utf-8"))
    globalvar.ui.table_account.setItem(0, 0, item)
    item = QTableWidgetItem(str(a.InvestorID, encoding="utf-8"))
    globalvar.ui.table_account.setItem(0, 1, item)
    item = QTableWidgetItem("%.2f" % a.Prebalance)
    globalvar.ui.table_account.setItem(0, 2, item)
    item = QTableWidgetItem("%.2f" % a.Current)
    globalvar.ui.table_account.setItem(0, 3, item)
    item = QTableWidgetItem(str(a.Rate) + '%')
    globalvar.ui.table_account.setItem(0, 4, item)
    item = QTableWidgetItem("%.2f" % a.Available)
    globalvar.ui.table_account.setItem(0, 5, item)
    item = QTableWidgetItem("%.2f" % a.Positionrate + '%')
    globalvar.ui.progressBar.setProperty("value", int(a.Positionrate))
    globalvar.ui.progressBar.setFormat("仓位：%p%")
    globalvar.ui.table_account.setItem(0, 6, item)
    # item = QTableWidgetItem("%.2f" % a.Commission)
    # VNDEFTradingAccountField
    item = QTableWidgetItem("Commission")
    globalvar.ui.table_account.setItem(0, 7, item)
    # item = QTableWidgetItem("%.2f" % a.WithdrawQuota)
    item = QTableWidgetItem("WithdrawQuota")
    globalvar.ui.table_account.setItem(0, 8, item)
    tstr = time.strftime("%Y-%m-%d %H:%M:%S ", time.localtime())
    tstr2 = time.strftime("%H%M%S ", time.localtime())
    item = QTableWidgetItem(tstr)
    globalvar.ui.table_account.setItem(0, 9, item)
    # cur_dir = str(ui.QDir.currentPath()) + "/strategyfile"
    TradingDaystr = str(a.TradingDay, encoding="utf-8")
    # if TradingDaystr == "":
    #    return
    # print(str(os.path.abspath(os.curdir)) + '\\curvedata\\' + str(a.TradingDay, encoding="utf-8") + '.txt')
    with open(str(os.path.abspath(os.curdir)) + '\\curvedata\\' + str(a.TradingDay, encoding="utf-8") + '.txt',
              "a") as f:
        f.write(str(a.TradingDay, encoding="utf-8") + ',' + tstr2 + ',' + str(
            a.Current) + '\n')  # 自带文件关闭功能，不需要再写f.close()
    global UIUpdaterealtimecurveThreadjs
    UIUpdaterealtimecurveThreadjs = UIUpdaterealtimecurveThreadjs + 1
    if UIUpdaterealtimecurveThreadjs >= 10 or UIUpdaterealtimecurveThreadjs == 0:
        UIUpdaterealtimecurveThreadjs = 0
        uithread = UIUpdaterealtimecurveThread(a.Current, a.TradingDay)
        uithread.start()


# 更新实时资金曲线
class UIUpdaterealtimecurveThread(threading.Thread):
    def __init__(self, newdata, tradeingday):
        super(UIUpdaterealtimecurveThread, self).__init__()
        self.newdata = newdata
        self.tradeingday = tradeingday

    def run(self):
        globalvar.ui.updaterealtimecurveUi(self.newdata, self.tradeingday)


# MyCTPTrade类继承自CTPTrader类
class MyCTPTrade(vnctptd, QtCore.QThread):
    def __init__(self, _signal):
        self._signal = _signal
        super().__init__(_signal)

    def updatetradestock(self, InstrumentID):
        tradestocklist = []
        for i in range(min(len(globalvar.ui.Button_t), len(tradestocklist))):
            globalvar.ui.Button_t[i].setText(str(tradestocklist[i]))
            if len(tradestocklist) > VN_MOUTHNUM:
                hstr = ''
                les = min(VN_MOUTHNUM, len(tradestocklist))
                for j in range(les):
                    if j < les - 1:
                        hstr = hstr + tradestocklist[i] + ','
                    else:
                        hstr = hstr + tradestocklist[i]
                # with open("historystock.ini", "w") as f:
                #    f.write(hstr)

    # 登录回调
    def OnRspUserLogin(self, a):
        # print(a.contents.a1, a.contents.a2)
        print(u'交易登录成功OnRspUserLogin')
        log_todaytd('交易登录成功OnRspUserLogin')
        globalvar.ui.SetBarTDState(3)

    # 退出登录回调
    def OnRspUserLogout(self, a):
        print(a.contents.a1, a.contents.a2)
        print(u'交易登出成功OnRspUserLogout')
        log_todaytd('交易登出成功OnRspUserLogout')

    # 建立连接回调
    def OnFrontConnected(self):
        print("连接交易服务器成功OnFrontConnected")
        log_todaytd('连接交易服务器成功OnFrontConnected')
        globalvar.ui.SetBarTDState(2)

        '''
        if self.ReqUserLogin() == 0:
            log_todaytd('发送登录交易服务器请求成功')
        else:
            log_todaytd('发送登录交易服务器请求失败')
        '''

    # 断开连接回调
    def OnFrontDisconnected(self, a):
        print("断开与交易服务器连接OnFrontDisconnected")
        log_todaytd('断开与交易服务器连接OnFrontDisconnected')
        globalvar.ui.SetBarTDState(1)

    # 请求查询投资者持仓响应
    def OnRspQryInvestorPosition(self, a):
        # print("请求查询投资者持仓响应OnRspQryInvestorPosition")
        update_position(a[0])

    # 账户资金回调
    def OnRspQryTradingAccount(self, a):
        print("账户资金回调OnRspQryTradingAccount")
        try:
            update_account(a[0])
        except Exception as e:
            print("OnRspQryTradingAccount Error:" + repr(e))

    # 委托回报
    def OnRtnOrder(self, a):
        print("----------------------委托回报OnRtnOrder")
        update_order(a)
        # updatetradestock(a.contents.InstrumentID)

    # 成交回报
    def OnRtnTrade(self, a):
        print("--------------------成交回报OnRtnTrade")
        update_trader(a)


class RegTdThreadOnFrontConnected(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnFrontConnected()


class RegTdThreadOnFrontDisconnected(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnFrontDisconnected()


class RegTdThreadOnRspUserLogin(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRspUserLogin()


class RegTdThreadOnRspUserLogout(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRspUserLogout()


class RegTdThreadOnRspQryInvestorPosition(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRspQryInvestorPosition()


class RegTdThreadOnRspQryTradingAccount(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRspQryTradingAccount()


class RegTdThreadOnRtnOrder(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRtnOrder()


class RegTdThreadOnRtnTrade(Thread):
    def __init__(self, name, td):
        super().__init__()
        self.name = name
        self.td = td

    def run(self):
        self.td.VNRegOnRtnTrade()


def function_td(td):
    RegTdThreadOnFrontConnected('OnFrontConnected', td).start()
    RegTdThreadOnFrontDisconnected('OnFrontDisconnected', td).start()
    RegTdThreadOnRspUserLogin('OnRspUserLogin', td).start()
    RegTdThreadOnRspUserLogout('OnRspUserLogout', td).start()
    RegTdThreadOnRspQryInvestorPosition('OnRspQryInvestorPosition', td).start()
    RegTdThreadOnRspQryTradingAccount('OnRspQryTradingAccount', td).start()
    RegTdThreadOnRtnOrder('OnRspQryInvestorPosition', td).start()
    RegTdThreadOnRtnTrade('OnRspQryInvestorPosition', td).start()

    time.sleep(1)
    td.InitTD()
    print("InitTD")
    # if td.InitTD() != 0:
    #    log_todaytd('初始化失败，请检查vnctptd.ini配置文件是否配置正确')
    # 调用交易接口元素，通过 “ 接口变量.元素（接口类内部定义的方法或变量） ” 形式调用
    # Login()，不需要参数，Login读取TD.ini的配置信息，并登录
    # 返回0， 表示登录成功，
    # 返回1， .ini错误
    # 返回2， 登录超时
    '''
    if td.ReqUserLogin() == 0:
        log_todaytd('发送登录交易服务器请求成功')
    else:
        log_todaytd('发送登录交易服务器请求失败')
    '''
    # 持仓数据在后台更新时，参数True为显示持仓状态，False为不显示持仓状态（仅对控制台有效）
    # td.SetShowPosition(True)

    # 注意simnow模拟盘的交易服务器不稳定，经常会出现查询不到的情况。实盘账户绑定的交易服务器无此问题。

    while 1:  # 死循环，反复执行
        '''
        # 如果值为-999999999（初始值），则表示尚未获得数据
        print(u'(1)动态权益：%0.02f' % td.QryBalance(True))
        print(u'(2)静态权益：%0.02f' % td.QryBalance(False))
        print(u'(3)可用资金：%0.02f' % td.QryAvailable())
        print(u'(4)zn1701今日空单持仓：%d' % td.QryPosition('rb1701', VN_POSITION_Sell_Today))
        print(u'(5)zn1701今日多单持仓：%d' % td.QryPosition('rb1701', VN_POSITION_Buy_Today))
        print(u'(6)zn1701非今日空单持仓：%d' % td.QryPosition('rb1701', VN_POSITION_Sell_History))
        print(u'(7)zn1701非今日多单持仓：%d' % td.QryPosition('rb1701', VN_POSITION_Buy_History))

        print('--------------------------------------------------------')
        '''
        time.sleep(3)  # sleep1秒，防止死循环导致CPU占有率过高，1即可，不宜过大，若过大会导致程序进程长时间无响应，丢失行情数据

    pass


class TDThread(QtCore.QThread):
    signal_td_tick = pyqtSignal(str)

    def __del__(self):
        self.wait()

    def __init__(self, tname):
        super(TDThread, self).__init__()
        self.tname = tname

    def run(self):
        time.sleep(1)
        globalvar.td = MyCTPTrade(self.signal_td_tick)
        globalvar.td.ui = globalvar.ui
        function_td(globalvar.td)


# 未使用的方法

global tablepositonnum
tablepositonnum = 0


def update_position(a):
    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tablepositonnum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetPositionNum()
            # print('持仓记录数a: ' + str(pnum))
            if tablepositonnum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetPosition(j)[0]
                    # print(returnvalue.contents.InstrumentID, returnvalue.contents.PosiDirection)
                    update_addposition(returnvalue)
            update_addposition(a)
            # print('add: ' + str(a.contents.InstrumentID, encoding="utf-8"),str(a.contents.PosiDirection, encoding="utf-8"))
            tablepositonnum = tablepositonnum + 1
    except Exception as e:
        print("update_position Error:" + repr(e))


# 更新持仓表格
def update_addposition(a):
    row_cnt = globalvar.ui.table_position.rowCount()
    thiskey = str(a.InstrumentID)
    if a.InstrumentID == '':
        return
    # 过滤平仓记录，不过滤仓位为0也会显示
    # 买平 1手 ，数据如下显示
    # 买卖，总持仓，可平量，持仓成本，持仓盈亏
    # 卖 ，0，    1，    0，       0
    if a.UseMargin < 1e-7:
        return
    if thiskey in globalvar.dict_position:
        thisrowid = row_cnt
        pass
    else:
        globalvar.dict_position[thiskey] = row_cnt
        globalvar.ui.table_position.insertRow(row_cnt)  # 尾部插入一行新行表格
        thisrowid = row_cnt
    column_cnt = globalvar.ui.table_position.columnCount()  # 返回当前列数
    item = QTableWidgetItem(str(a.InstrumentID, encoding="utf-8"))
    # md.SubscribeMarketData(a.InstrumentID)
    globalvar.ui.table_position.setItem(thisrowid, 0, item)
    # print('PosiDirection: '+ str(a.PosiDirection, encoding="utf-8"))
    # 净
    # define THOST_FTDC_PD_Net '1'
    # 多头
    # define THOST_FTDC_PD_Long '2'
    # 空头
    # define THOST_FTDC_PD_Short '3'
    # item = QTableWidgetItem(str(a.PosiDirection, encoding="utf-8"))
    if a.PosiDirection == b'1':
        item = QTableWidgetItem('净')
    elif a.PosiDirection == b'2':
        item = QTableWidgetItem('买')
    elif a.PosiDirection == b'3':
        item = QTableWidgetItem('卖')
    globalvar.ui.table_position.setItem(thisrowid, 1, item)
    item = QTableWidgetItem(str(a.Position))  # 持仓
    globalvar.ui.table_position.setItem(thisrowid, 2, item)
    item = QTableWidgetItem(str(a.OpenVolume))  # 今仓
    globalvar.ui.table_position.setItem(thisrowid, 3, item)
    item = QTableWidgetItem(str(a.Position - a.CloseVolume))  # 可平仓
    globalvar.ui.table_position.setItem(thisrowid, 4, item)
    item = QTableWidgetItem("%.2f" % a.UseMargin)  # 保证金
    globalvar.ui.table_position.setItem(thisrowid, 5, item)
    item = QTableWidgetItem("%.2f" % a.PositionCost)  # 持仓成本
    globalvar.ui.table_position.setItem(thisrowid, 6, item)
    item = QTableWidgetItem("%.2f" % a.PositionProfit)  # 持仓盈亏
    globalvar.ui.table_position.setItem(thisrowid, 7, item)
    '''
    ui.Trade_CancelBtn  = QtWidgets.QPushButton('双击人工平仓')
    ui.Trade_CancelBtn.setFlat(True)
    ui.Trade_CancelBtn.setStyleSheet('background-color:#ff0000;');
    # searchBtn.setDown(True)
    ui.Trade_CancelBtn.setStyleSheet('QPushButton{margin:3px}')
    ui.table_position.setCellWidget(thisrowid, 6, ui.Trade_CancelBtn)
    '''


def update3table():
    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tablepositonnum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetPositionNum()
            print('持仓记录数c: ' + str(pnum))
            if tablepositonnum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetPosition(j)
                    print(returnvalue.contents.InstrumentID, returnvalue.contents.PosiDirection)
                    update_addposition(returnvalue)
            tablepositonnum = tablepositonnum + 1
    except Exception as e:
        print("update3table1 Error:" + repr(e))

    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tableontradenum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetOnTradeNum()
            print('交易记录数c: ' + str(pnum))
            if tableontradenum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetOnTrade(j)
                    print(returnvalue.contents.InstrumentID, str(returnvalue.contents.Volume))
                    update_addontrade(returnvalue)
            tableontradenum = tableontradenum + 1
    except Exception as e:
        print("update3table2 Error:" + repr(e))

    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tableonordernum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetOnOrderNum()
            print('委托记录c: ' + str(pnum))
            if tableonordernum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetOnOrder(j)
                    print(returnvalue.contents.InstrumentID, returnvalue.contents.OrderRef)
                    update_addonorder(returnvalue)
            tableonordernum = tableonordernum + 1
    except Exception as e:
        print("update3table3 Error:" + repr(e))


# 更新交易记录表格
global tableontradenum
tableontradenum = 0


def update_addontrade(a):
    row_cnt = globalvar.ui.table_trade.rowCount()
    thiskey = str(a.contents.InstrumentID)
    if a.contents.InstrumentID == '':
        return
    if a.contents.UseMargin < 1e-7:
        return
    if thiskey in globalvar.dict_position:
        thisrowid = row_cnt
        pass
    else:
        globalvar.dict_position[thiskey] = row_cnt
        globalvar.ui.table_trade.insertRow(row_cnt)  # 尾部插入一行新行表格
        thisrowid = row_cnt
    column_cnt = globalvar.ui.table_trade.columnCount()  # 返回当前列数
    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))
    # md.SubscribeMarketData(a.contents.InstrumentID)
    globalvar.ui.table_trade.setItem(thisrowid, 0, item)

    item = QTableWidgetItem(str(a.contents.TradeID, encoding="utf-8"))  # 成交编号
    globalvar.ui.table_trade.setItem(thisrowid, 1, item)

    item = QTableWidgetItem(str(a.contents.OrderSysID, encoding="utf-8"))  # 报单编号
    globalvar.ui.table_trade.setItem(thisrowid, 2, item)

    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))  # 合约
    globalvar.ui.table_trade.setItem(thisrowid, 3, item)

    if a.contents.PosiDirection == b'1':
        item = QTableWidgetItem('净')
    elif a.contents.PosiDirection == b'2':
        item = QTableWidgetItem('买')
    elif a.contents.PosiDirection == b'3':
        item = QTableWidgetItem('卖')
    globalvar.ui.table_trade.setItem(thisrowid, 4, item)

    item = QTableWidgetItem(str(a.contents.OffsetFlag, encoding="utf-8"))  # 开平
    globalvar.ui.table_trade.setItem(thisrowid, 5, item)

    item = QTableWidgetItem(str(a.contents.Price, encoding="utf-8"))  # 成交价格
    globalvar.ui.table_trade.setItem(thisrowid, 6, item)

    item = QTableWidgetItem(str(a.contents.Volume))  # 委托手数
    globalvar.ui.table_trade.setItem(thisrowid, 7, item)


def update_ontrade(a):
    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tableontradenum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetOnTradeNum()
            # print('交易记录数: ' + str(pnum))
            if tableontradenum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetOnTrade(j)
                    # print(returnvalue.contents.InstrumentID, returnvalue.contents.PosiDirection)
                    update_addontrade(returnvalue)
            update_addontrade(a)
            tableontradenum = tableontradenum + 1
    except Exception as e:
        print("update_ontrade Error:" + repr(e))


# 更新委托记录表格
global tableonordernum
tableonordernum = 0


def update_addonorder(a):
    row_cnt = globalvar.ui.table_order.rowCount()
    thiskey = str(a.contents.InstrumentID)
    if a.contents.InstrumentID == '':
        return
    if a.contents.UseMargin < 1e-7:
        return
    if thiskey in globalvar.dict_position:
        thisrowid = row_cnt
        pass
    else:
        globalvar.dict_position[thiskey] = row_cnt
        globalvar.ui.table_order.insertRow(row_cnt)  # 尾部插入一行新行表格
        thisrowid = row_cnt
    column_cnt = globalvar.ui.table_order.columnCount()  # 返回当前列数

    item = QTableWidgetItem(str(a.contents.OrderSysID, encoding="utf-8"))
    globalvar.ui.table_order.setItem(thisrowid, 0, item)

    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))
    # md.SubscribeMarketData(a.contents.InstrumentID)
    globalvar.ui.table_order.setItem(thisrowid, 1, item)
    if a.contents.Direction == '0':
        item = QTableWidgetItem("多")  # 方向
    else:
        item = QTableWidgetItem("空")  # 方向
    globalvar.ui.table_order.setItem(thisrowid, 2, item)
    if a.contents.CombOffsetFlag == '0':
        item = QTableWidgetItem("开")
    else:
        item = QTableWidgetItem("平")
    globalvar.ui.table_order.setItem(thisrowid, 3, item)
    item = QTableWidgetItem(str(a.contents.OrderStatus))  # 报单状态
    globalvar.ui.table_order.setItem(thisrowid, 4, item)
    item = QTableWidgetItem(str(a.contents.VolumeTotalOriginal))  # 数量
    globalvar.ui.table_order.setItem(thisrowid, 5, item)
    item = QTableWidgetItem(str(a.contents.Position - a.contents.VolumeTraded))  # 今成交量
    globalvar.ui.table_order.setItem(thisrowid, 6, item)
    item = QTableWidgetItem(str(a.contents.LimitPrice))  # 价格
    globalvar.ui.table_order.setItem(thisrowid, 7, item)
    item = QTableWidgetItem("%s" % a.contents.InsertTime)  # 报单时间
    globalvar.ui.table_order.setItem(thisrowid, 8, item)
    item = QTableWidgetItem("%s" % a.contents.ExchangeID)  # 交易所
    globalvar.ui.table_order.setItem(thisrowid, 9, item)


def update_onorder(a):
    # 检查表格记录数和C++底层数据是否一致，如果不一致，直接从底层CPP变量读取记录，否则仅增加一条记录
    global tableonordernum
    try:
        if globalvar.td:
            pnum = globalvar.td.GetOnOrderNum()
            # print('委托记录数: ' + str(pnum))
            if tableonordernum < pnum:
                for j in range(pnum):
                    returnvalue = globalvar.td.GetOnOrder(j)
                    # print(returnvalue.contents.InstrumentID, returnvalue.contents.PosiDirection)
                    update_addonorder(returnvalue)
            update_addonorder(a)
            tableonordernum = tableonordernum + 1
    except Exception as e:
        print("update_onorder Error:" + repr(e))


def update_order(a):
    row_cnt = globalvar.ui.table_order.rowCount()
    thiskey = str(a.contents.InstrumentID)
    if a.contents.InstrumentID == '':
        return
    if thiskey in globalvar.dict_order:
        thisrowid = row_cnt
        pass
    else:
        print("y: " + thiskey)
        globalvar.dict_order[thiskey] = row_cnt
        globalvar.ui.table_order.insertRow(row_cnt)  # 尾部插入一行新行表格
        thisrowid = row_cnt
    column_cnt = globalvar.ui.table_order.columnCount()  # 本地报单编号
    item = QTableWidgetItem(str(a.contents.OrderLocalID, encoding="utf-8"))
    globalvar.ui.table_order.setItem(thisrowid, 0, item)
    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))
    globalvar.ui.table_order.setItem(thisrowid, 1, item)
    if a.contents.Direction == '0':
        item = QTableWidgetItem('买')
    else:
        item = QTableWidgetItem('卖')
    globalvar.ui.table_order.setItem(thisrowid, 2, item)
    item = QTableWidgetItem(str(a.contents.CombOffsetFlag, encoding="utf-8"))  # 开平
    globalvar.ui.table_order.setItem(thisrowid, 3, item)
    item = QTableWidgetItem(str(a.contents.OrderStatus, encoding="utf-8"))  # 报单状态
    globalvar.ui.table_order.setItem(thisrowid, 4, item)
    item = QTableWidgetItem("%d" % a.contents.VolumeTotalOriginal)  # 报单手数
    globalvar.ui.table_order.setItem(thisrowid, 5, item)
    item = QTableWidgetItem("%d" % a.contents.VolumeTraded)  # 成交手数
    globalvar.ui.table_order.setItem(thisrowid, 6, item)


def update_trader(a):
    row_cnt = globalvar.ui.table_trade.rowCount()
    thiskey = str(a.contents.InstrumentID)
    if a.contents.InstrumentID == '':
        return
    if thiskey in globalvar.dict_trader:
        thisrowid = row_cnt
        pass
    else:
        print("y: " + thiskey)
        globalvar.ict_trader[thiskey] = row_cnt
        globalvar.ui.table_trade.insertRow(row_cnt)  # 尾部插入一行新行表格
        thisrowid = row_cnt
    # column_cnt = ui.table_trade.columnCount()  # 返回当前列数
    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))
    globalvar.ui.table_trade.setItem(thisrowid, 0, item)
    item = QTableWidgetItem(str(a.contents.TradeID, encoding="utf-8"))  # 成交编号
    globalvar.ui.table_trade.setItem(thisrowid, 1, item)
    item = QTableWidgetItem(str(a.contents.OrderSysID, encoding="utf-8"))  # 报单编号
    globalvar.ui.table_trade.setItem(thisrowid, 2, item)
    item = QTableWidgetItem(str(a.contents.InstrumentID, encoding="utf-8"))  # 合约
    globalvar.ui.table_trade.setItem(thisrowid, 3, item)
    if a.contents.PosiDirection == b'1':
        item = QTableWidgetItem('净')
    elif a.contents.PosiDirection == b'2':
        item = QTableWidgetItem('买')
    elif a.contents.PosiDirection == b'3':
        item = QTableWidgetItem('卖')
    globalvar.ui.table_trade.setItem(thisrowid, 4, item)
    item = QTableWidgetItem(str(a.contents.OffsetFlag, encoding="utf-8"))  # 开平
    globalvar.ui.table_trade.setItem(thisrowid, 5, item)
    item = QTableWidgetItem(str(a.contents.Price, encoding="utf-8"))  # 成交价格
    globalvar.ui.table_trade.setItem(thisrowid, 6, item)
    item = QTableWidgetItem(str(a.contents.Volume))  # 委托手数
    globalvar.ui.table_trade.setItem(thisrowid, 7, item)
