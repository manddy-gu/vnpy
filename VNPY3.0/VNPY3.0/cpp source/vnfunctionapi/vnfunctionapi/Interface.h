/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#pragma once

#ifdef PYCTPMARKET_EXPORTS
#define VN_EXPORT __declspec(dllexport)
#else
#define VN_EXPORT __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"{
#endif 


	void VN_EXPORT AsynSleep(DWORD dwMilliseconds);

#ifdef __cplusplus
}
#endif