/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#include "stdafx.h"
#include "MdSpi.h"
#include "mapdef.h"
#include <map>
#include <string>
#include <algorithm>
#include "iostream"
#include <iostream> 
#include <windows.h>
#include "TickToKline.h"
using namespace std;

CThostFtdcMdApi *mpUserApi = NULL;
CMdSpi vnmdspi;
#pragma warning(disable : 4996)

extern std::string gFrontAddr[3];
extern std::string gBrokerID;
extern std::string gInvestorID;
extern std::string gPassword;

extern char* ppInstrumentID[];	
extern int amount;
extern KLineDataType klinedata[MAX_INSUTRUMENT][MAX_KLINE];
extern std::hash_map<std::string, int> gMarket;

// 不同合约的k线存储表
hash_map<string, CTickToKline> g_KlineHash;
int maxv = 0;
extern double  UpdateKline(string instrumentKey, int i, int id);

int  GetMin(double thistime)
{
	int h = (int)(thistime * 100); //  21
	int m = (int)(thistime * 10000) - 100 * h; //  2130-2100=30
	return  (h * 60 + m);
}
string dbtoch(double nums)		//将double 转换为 string 进行保存
{
	char chr[20] = { 0 };
	sprintf(chr, "%.0f", nums);
	string tt = chr;
	return tt;
}


void WirteSingleRecordToFile8(int id, const char * content)
{
	char str[200] = { 0 };
	strcat_s(str, 200, "TradeRecord.txt");
	ifstream inf;
	ofstream ouf;
	inf.open(str, ios::out);
	ofstream o_file(str, ios::app);
	o_file << content << "\t" << endl;
	o_file.close();		
}

inline bool  CheckData(double data)
{
	if ((!_isnan(data)) && data > 1e-10)
	{
		return true;
	}
	return false;
}
inline bool  CheckDataOnlyNum(double data)
{
	if (!_isnan(data))
	{
		return true;
	}
	return false;
}
double zero_max(double a, double b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0)
	{
		return b;
	}
	else if (b == 0)
	{
		return a;
	}
	else
	{
		return max(a, b);
	}
}

double zero_min(double a, double b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0)
	{
		return b;
	}
	else if (b == 0)
	{
		return a;
	}
	else
	{
		return min(a, b);
	}
}
bool CheckFirst(double time, int cycle)
{
	int temp = (int)(time * 10000) - 100 * int(time * 100);
	if (temp % cycle == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

DWORD WINAPI QryThreadProc(void* p)	//更新排名
{
	return 1;
	while (true)
	{
		CThostFtdcDepthMarketDataField  obj  ;
		memset(&obj, 0, sizeof(CThostFtdcDepthMarketDataField));
		_snprintf_s(obj.InstrumentID, sizeof(TThostFtdcInstrumentIDType), sizeof(TThostFtdcInstrumentIDType) - 1, "%s", "rb2110");
		obj.LastPrice=3500.0;
 		vnmdspi.PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRtnDepthMarketData, &obj, NULL, 0);
		Sleep(1000);
	}
}

void CMdSpi::PMsg(unsigned nThreadID, int msg, LPVOID p1, LPVOID p2, int Reason)
{
	switch (msg)
	{
	case MY_OnFrontConnected:
		if (!::PostThreadMessage(nThreadID, msg, 0, 0))
		{
			printf("post message(OnFrontConnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnFrontDisconnected:
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)Reason, NULL))
		{
			printf("post message(MY_OnFrontDisconnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnRspUserLogin:
	{
		CThostFtdcRspUserLoginField * t1 = new CThostFtdcRspUserLoginField;
		memset(t1, 0, sizeof(CThostFtdcRspUserLoginField));
		memcpy_s(t1, sizeof(CThostFtdcRspUserLoginField),p1,sizeof(CThostFtdcRspUserLoginField));
		/*
		CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		memcpy_s(t2, sizeof(CThostFtdcRspInfoField), p2, sizeof(CThostFtdcRspInfoField));
		*/
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			//delete t2;
			t1 = NULL;
			//t2 = NULL;
			printf("post message(MY_OnRspUserLogin) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspUserLogout:
	{
		CThostFtdcUserLogoutField * t1 = new CThostFtdcUserLogoutField;
		memset(t1, 0, sizeof(CThostFtdcUserLogoutField));
		memcpy_s(t1, sizeof(CThostFtdcUserLogoutField), p1, sizeof(CThostFtdcUserLogoutField));
		/*
		CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		memcpy_s(t2, sizeof(CThostFtdcRspInfoField), t2, sizeof(CThostFtdcRspInfoField));
		*/
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			//delete t2;
			t1 = NULL;
			//t2 = NULL;
			printf("post message(MY_OnRspUserLogin) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspQryInvestorPosition:
	{
		break;
	}
	case MY_OnRspQryTradingAccount:
	{
		break;
	}
	case MY_OnRtnOrder:
	{
		break;
	}
	case MY_OnRtnTrade:
	{
		break;
	}
	case MY_OnRtnDepthMarketData:
	{
		CThostFtdcDepthMarketDataField * t1 = new CThostFtdcDepthMarketDataField;
		memset(t1, 0, sizeof(CThostFtdcDepthMarketDataField));
		memcpy_s(t1, sizeof(CThostFtdcDepthMarketDataField), p1, sizeof(CThostFtdcDepthMarketDataField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			//printf("post message(MY_OnRtnDepthMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspSubMarketData:
	{
		CThostFtdcSpecificInstrumentField * t1 = new CThostFtdcSpecificInstrumentField;
		memset(t1, 0, sizeof(CThostFtdcSpecificInstrumentField));
		memcpy_s(t1, sizeof(CThostFtdcSpecificInstrumentField), p1, sizeof(CThostFtdcSpecificInstrumentField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			//printf("post message(MY_OnRspSubMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspUnSubMarketData:
	{
		CThostFtdcSpecificInstrumentField * t1 = new CThostFtdcSpecificInstrumentField;
		memset(t1, 0, sizeof(CThostFtdcSpecificInstrumentField));
		memcpy_s(t1, sizeof(CThostFtdcSpecificInstrumentField), p1, sizeof(CThostFtdcSpecificInstrumentField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			//printf("post message(MY_OnRspUnSubMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspForQuote:
	{
		break;
	}
	case MY_OnRspAuthenticate:
	{
		break;
	}
	case MY_IsErrorRspInfo:
	{
		break;
	}
  }
}

CMdSpi::CMdSpi()
{
	memset(klinedata, 0, sizeof(KLineDataType)*MAX_INSUTRUMENT*MAX_KLINE);
	begintime1=-1;
	begintime2=-1;
	begintime3=-1;
	begintime4=-1;
	endtime1=-1;
	endtime2=-1;
	endtime3=-1;
	endtime4=-1;
	mInitOK = FALSE;
	mhSyncObj = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	RequestID = 0;
}

CMdSpi::~CMdSpi()
{
	::CloseHandle(mhSyncObj);
}

void CMdSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo,int nRequestID, bool bIsLast)
{
	if(showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	IsErrorRspInfo(pRspInfo);
}

void CMdSpi::OnFrontDisconnected(int nReason)
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnFrontDisconnected, MY_OnFrontDisconnected, NULL, NULL, nReason);
	::Beep(800, 500);
}

void CMdSpi::OnHeartBeatWarning(int nTimeLapse)
{
}

BOOL CMdSpi::Init()
{
	return TRUE;
}

void CMdSpi::OnFrontConnected()
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnFrontConnected, MY_OnFrontConnected, NULL, NULL, 0);
    ///用户登录请求
	ReqUserLogin();	  
}

char *CMdSpi::GetApiVersion()
{
	_snprintf_s(ver,sizeof(ver),sizeof(ver),"%s", (char*)(mpUserApi->GetApiVersion()));
	return ver;
}

char *CMdSpi::GetTradingDay()
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	_snprintf_s(tradingday, sizeof(tradingday), sizeof(tradingday), "%s", (char*)(mpUserApi->GetTradingDay()));
	return tradingday;
}

void   CMdSpi::RegisterFront(char *pszFrontAddress)
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	mpUserApi->RegisterFront(pszFrontAddress);
}

void   CMdSpi::RegisterNameServer(char *pszNsAddress)
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	mpUserApi->RegisterNameServer(pszNsAddress);
}

int CMdSpi::ReqUserLogin()
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, gBrokerID.c_str());
	strcpy(req.UserID, gInvestorID.c_str());
	strcpy(req.Password, gPassword.c_str());
	printf("md login: %s,%s,%s \n",req.BrokerID, req.UserID, req.Password);

	return mpUserApi->ReqUserLogin(&req, ++RequestID);
}

int CMdSpi::ReqUserLogout()
{
	if (showlog)
	cerr << "--->>> " << __FUNCTION__ << std::endl;
 	CThostFtdcUserLogoutField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, gBrokerID.c_str());
	strcpy(req.UserID, gInvestorID.c_str());
	//strcpy(req.Password, gPassword.c_str());
	return mpUserApi->ReqUserLogout(&req, ++RequestID);
}

void CMdSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << "--->>> " << __FUNCTION__ << std::endl;
	if (!pRspUserLogin)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogin, MY_OnRspUserLogin, pRspUserLogin, pRspInfo, nRequestID);

	return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		///获取当前交易日
		cerr << "TradeingDay: " << mpUserApi->GetTradingDay() << endl;
		mInitOK = TRUE;
	   if (pRspInfo && pRspInfo->ErrorID != 0)
	   {
			printf("OnRspUserLogin Failer,ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);

			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			memcpy_s(&tn, sizeof(CThostFtdcRspUserLoginField), pRspUserLogin, sizeof(CThostFtdcRspUserLoginField));
	   }
	   else
	   {
			printf("OnRspUserLogin Scuess\n");
			Sleep(3000);
	   }
	}
}

void CMdSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << "--->>> " << __FUNCTION__ << std::endl;
	if (!pUserLogout)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogout, MY_OnRspUserLogout, pUserLogout, 0, 0);
}

int CMdSpi::SubscribeForQuoteRsp(char * InstrumentID)
{
	return mpUserApi->SubscribeForQuoteRsp(&InstrumentID,1);
}

int ttid = 0;
int CMdSpi::SubscribeMarketData(char * InstrumentID)
{
    char Instrument_All_Name_Str[1][31] = {""};
    char * Instrument_All_Name[1];
	_snprintf_s(&Instrument_All_Name_Str[0][31], 31, 30, "%s", InstrumentID);
	Instrument_All_Name[0] = &Instrument_All_Name_Str[0][31];
	if (Instrument_All_Name[0])
	{
		int iResult = mpUserApi->SubscribeMarketData(&(Instrument_All_Name[0]), 1);
	}
}

int CMdSpi::SubscribeMarketData()
{
	return mpUserApi->SubscribeMarketData( &(ppInstrumentID[amount - 1]), 1);
}

int CMdSpi::UnSubscribeMarketData()
{
	if (amount >= 1)
	{
		int iResult = mpUserApi->UnSubscribeMarketData(&(ppInstrumentID[amount - 1]), 1);
		if (iResult != 0)
			cerr << "Failer:" << __FUNCTION__ << ppInstrumentID[amount - 1] << endl;
		else
			cerr << "Scuess:" << __FUNCTION__ << ppInstrumentID[amount - 1] << endl;
	}
	return 0;
}

bool firststate = true;

//加锁
HANDLE hThread[12];
unsigned int uiThreadId[12];
//加锁
/*
DWORD WINAPI strategy(const LPVOID lpParam)
//UINT strategy(LPVOID pParam)
// void CMdSpi::strategy()
{
	Py_Initialize();

	PyObject * pModule = NULL;
	PyObject * pFunc = NULL;
	pModule = PyImport_ImportModule("Test001");	    //Test001:Python文件名
	pFunc = PyObject_GetAttrString(pModule, "TestDict");	//Add:Python文件中的函数名
															//创建参数:
	PyObject *pArgs = PyTuple_New(1);
	PyObject *pDict = PyDict_New();   //创建字典类型变量
	PyDict_SetItemString(pDict, "Name", Py_BuildValue("s", "WangYao")); //往字典类型变量中填充数据
	PyDict_SetItemString(pDict, "Age", Py_BuildValue("i", 25));         //往字典类型变量中填充数据
	PyTuple_SetItem(pArgs, 0, pDict);//0---序号  将字典类型变量添加到参数元组中
									 //返回值
	PyObject *pReturn = NULL;
	pReturn = PyEval_CallObject(pFunc, pArgs);	    //调用函数
													//处理返回值:
	Py_ssize_t size = PyDict_Size(pReturn);
	cout << "返回字典的大小为: " << size << endl;
	PyObject *pNewAge = PyDict_GetItemString(pReturn, "Age");
	int newAge;
	PyArg_Parse(pNewAge, "i", &newAge);
	cout << "True Age: " << newAge << endl;

	Py_Finalize();
	return 1;
};

unsigned int __stdcall SniffAndTradingThread(void *lpParam)
{
	if (lpParam)
	{
		idrand* ir = (idrand*)lpParam;//获取参数结构体
		while (true)
		{
			showcalnum[ir->threadid]++;
			if (showcalnum[ir->threadid] > 200)
			{
				showcalnum[ir->threadid] = 0;
				printf("[%d]  %d~%d\n", ir->threadid, ir->beginid, ir->endid);
			}


			// WaitForSingleObject(semaphorehandle[ir->threadid], INFINITE);
			Sniffer(ir->beginid, ir->endid);
			Trading(ir->beginid, ir->endid);	//冲突

			::Sleep(100);
			//  ReleaseSemaphore(semaphorehandle[ir->threadid], 1, NULL);
		}
		delete lpParam;
		lpParam = NULL;
	}
	return 0;
}
*/

double GetLocalTimeSec2()
{
	SYSTEMTIME sys_time;
	GetLocalTime(&sys_time);
	double system_times;
	system_times = (double)((sys_time.wHour) / 10e1) + (double)((sys_time.wMinute) / 10e3) + (double)((sys_time.wSecond) / 10e5);	//格式时间0.145100
	return system_times;
}



void CMdSpi::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{	
	if (!pDepthMarketData){return;}
	// 计算实时k线
	std::string instrumentKey = std::string(pDepthMarketData->InstrumentID);
	if (g_KlineHash.find(instrumentKey) == g_KlineHash.end())
		g_KlineHash[instrumentKey] = CTickToKline();

	if (g_KlineHash[instrumentKey].KLineM1FromTick(pDepthMarketData))
	{
		//生成新的M1 K线数据成功
		size_t count = g_KlineHash[instrumentKey].m_KLineM1.size();
		 if (count > 0)
		{
			if (gMarket[pDepthMarketData->InstrumentID] > MAX_INSUTRUMENT){return;}
			memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][0], sizeof(KLineDataType), 
				&g_KlineHash[instrumentKey].m_KLinelastM1, sizeof(KLineDataType));

			for (size_t i = 0; i <  min(count, MAX_KLINE); i++)
			{
				//memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][i + 1], sizeof(KLineDataType),

				memcpy_s(&klinedata[gMarket[pDepthMarketData->InstrumentID]][count + 1 - (int)(i + 1)], sizeof(KLineDataType),
					
					&g_KlineHash[instrumentKey].m_KLineM1[i], sizeof(KLineDataType));

				//printf("UpdateKline: %s, %d , %d\n", instrumentKey.c_str(), gMarket[pDepthMarketData->InstrumentID], (int)(i + 1));
				//UpdateKline(instrumentKey, gMarket[pDepthMarketData->InstrumentID],(int)(i+1));
				UpdateKline(instrumentKey, gMarket[pDepthMarketData->InstrumentID], count + 1 - (int)(i + 1));

				maxv = max(maxv, gMarket[pDepthMarketData->InstrumentID]);
				/*printf("g_KlineHash size: %d [%d] [max : %d]\n", (int)g_KlineHash[instrumentKey].m_KLineM1.size(), gMarket[pDepthMarketData->InstrumentID], maxv);
				printf("ss[%d]: %s %s %.6f  %.2f \n",(int)(i),pDepthMarketData->InstrumentID,
					g_KlineHash[instrumentKey].m_KLineM1[i].TradingDay,g_KlineHash[instrumentKey].m_KLineM1[i].KlineTime,g_KlineHash[instrumentKey].m_KLineM1[i].ClosePrice
				);
				*/
			}
		}
	}

	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRtnDepthMarketData, pDepthMarketData, NULL, 0);
}

bool CMdSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		SYSTEMTIME t;
		::GetLocalTime(&t);
		std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
		std::cout << "--->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << std::endl;
	}
	return bResult;
}

///订阅行情应答
void  CMdSpi::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << "--->>> " << __FUNCTION__ << std::endl;

	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRspSubMarketData, pSpecificInstrument, NULL, 0);
};

///取消订阅行情应答
void  CMdSpi::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (showlog)
		cerr << "--->>> " << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRspUnSubMarketData, pSpecificInstrument, NULL, 0);
};

///询价通知
void  CMdSpi::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp)
{
	if (showlog)
		cerr << "--->>> " << __FUNCTION__ << std::endl;
};